/**
 * Manages file IO via streams.
 */
export abstract class AbstractStorageManager {
  /**
   *
   */
  public abstract saveFile(): any;
}
