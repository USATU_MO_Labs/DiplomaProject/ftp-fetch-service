import PQueue from "p-queue";
import FtpClient from "promise-ftp";
import urlJoin from "url-join";

export interface IFtpFile {
  /** Path on FTP server */
  path: string;

  /** File name */
  name: string;

  /** Size in bytes */
  size: number;
}

export enum IFileFormat {
  ImagCDF,
  IAGA2002,
}

export interface IParsedFileName {
  /** Observatory code */
  observatory: string;

  /** Date that the data corresponds to */
  date: Date;

  /** File format */
  format: IFileFormat;
}

export type IFile = IFtpFile & IParsedFileName;

type RecursiveMapCallback = (file: IFtpFile) => void;

export class IntermagnetFtpClient {
  /**
   * Create and initialize an Intermagnet FTP client
   */
  public static async create(): Promise<IntermagnetFtpClient> {
    const client = new IntermagnetFtpClient();
    await client.init();
    return client;
  }

  private ftpClient: FtpClient;

  private constructor() {
    this.ftpClient = new FtpClient();
  }

  /**
   * Returns flat list of all files with observatory data.
   * Includes ImagCDF and IAGA2002 only. All other files, if present, are ignored.
   */
  public async getRecursiveFlatList(): Promise<IFile[]> {
    const rootPath = "/intermagnet/minute/quasi-definitive";
    const files: IFile[] = [];

    await this.recursiveMap(rootPath, (file) => {
      const parsedFileName = this.parseFileName(file.name);

      if (parsedFileName) {
        files.push(Object.assign({}, file, parsedFileName));
      }
    });

    return files;
  }

  public streamFile(file: IFile): Promise<ReadableStream> {
    return this.ftpClient.get(file.path) as Promise<ReadableStream>;
  }

  /**
   * Close the connection to the ftp server after any/all enqueued commands have been executed.
   */
  public end() {
    return this.ftpClient.end() as Promise<boolean | Error>;
  }

  /**
   * Recursively walks the directory tree starting on a given root directory.
   * Applies given callback to each file found.
   */
  private async recursiveMap(rootPath: string, callback: RecursiveMapCallback) {
    const queue = new PQueue({
      autoStart: true,
      concurrency: 10,
    });

    const ftpClient = this.ftpClient;

    async function processPath(path: string) {
      const list = await ftpClient.list(path);
      list.forEach((item) => {
        const childPath = urlJoin(path, item.name);
        if (item.type === "d") {
          // if directory, add for recursive processing
          console.log(`Directory ${childPath}`);
          queue.add(async () => {
            await processPath(childPath);
          });
        } else if (item.type === "-") {
          // if file, call callback
          callback({
            path: childPath,
            name: item.name,
            size: parseInt(item.size, 10) || -1,
          });
        } else {
          // if symlink, skip
          console.warn(`Symlink found at '${childPath}', skipping...`);
        }
      });
    }

    processPath.bind(this);

    await processPath(rootPath);

    return queue.onIdle();
  }

  private parseFileName(filename: string): IParsedFileName | null {
    // ImagCDF: thy_20190525_0000_pt1m_3.cdf
    const cdfRegex = /^([a-z]{3})_(\d{4})(\d{2})(\d{2})_(\d{4})_(\w{4})_(\d)\.cdf$/;
    const cdfMatch = filename.match(cdfRegex);

    // IAGA2002: aae20110101qmin.min
    const iagaRegex = /^([a-z]{3})(\d{4})(\d{2})(\d{2})qmin.min$/;
    const iagaMatch = filename.match(iagaRegex);

    let result: IParsedFileName | null;
    if (cdfMatch) {
      const year = parseInt(cdfMatch[2], 10);
      const month = parseInt(cdfMatch[3], 10);
      const day = parseInt(cdfMatch[4], 10);
      result = {
        observatory: cdfMatch[1],
        date: new Date(year, month, day),
        format: IFileFormat.ImagCDF,
      };
    } else if (iagaMatch) {
      const year = parseInt(iagaMatch[2], 10);
      const month = parseInt(iagaMatch[3], 10);
      const day = parseInt(iagaMatch[4], 10);
      result = {
        observatory: iagaMatch[1],
        date: new Date(year, month, day),
        format: IFileFormat.IAGA2002,
      };
    } else {
      result = null;
    }

    return result;
  }

  private init(): Promise<void> {
    return this.ftpClient.connect({
      host: "ftp.seismo.nrcan.gc.ca",
    }) as Promise<void>;
  }
}
