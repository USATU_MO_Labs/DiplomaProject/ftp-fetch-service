export {
  AbstractFileRegistry as FileRegistry,
  FileStatus,
  IRegistryRecord,
  IRegistryItem,
} from "./abstract-file-registry";
export { FSFileRegistry } from "./fs-file-registry";
export { GCPFileRegistry } from "./gcp-file-registry";
