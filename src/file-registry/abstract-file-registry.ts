import { IFileFormat } from "../intermagnet-ftp-client";

/**
 * Marks files as sent for processing or not yet sent
 */
export enum FileStatus {
  /** File was saved, but was not sent for processing yet */
  NOT_PROCESSED = 0,

  /** File was sent for processing */
  PROCESSED = 1,
}

export interface IRegistryRecord {
  /** Date the files correspond to */
  date: Date;

  /** Files with data from observatories */
  items: IRegistryItem[];
}

export interface IRegistryItem {
  /** File name */
  name: string;

  /** Observatory code */
  observatory: string;

  /** Date the data corresponds to */
  date: Date;

  /** Id in storage; used to retrieve the file from storage */
  storageFileId: string;

  /** File status (processed / not processed) */
  status: FileStatus;

  /** File format */
  format: IFileFormat;
}

export interface IRegistryRecordSerialized {
  date: string;
  items: IRegistryItemSerialized[];
}

export interface IRegistryItemSerialized {
  name: string;
  observatory: string;
  date: string;
  storageFileId: string;
  status: number;
  format: IFileFormat;
}

/**
 * Registry duplicates the Map behaviour. The class provides hooks for read/write operations.
 * These hooks should be used for communicating with some kind of storage that backs up the data.
 */
export abstract class AbstractFileRegistry
  implements Map<Date, IRegistryRecord> {
  public get size(): number {
    return this.internalMap.size;
  }

  protected static deserializeRecord(
    record: IRegistryRecordSerialized,
  ): IRegistryRecord {
    return {
      date: new Date(record.date),
      items: record.items.map((item) => ({
        ...item,
        date: new Date(item.date),
      })),
    };
  }

  protected static serializeRecord(
    record: IRegistryRecord,
  ): IRegistryRecordSerialized {
    return {
      date: record.date.toDateString(),
      items: record.items.map((item) => ({
        ...item,
        date: item.date.toDateString(),
      })),
    };
  }

  public [Symbol.toStringTag]: string;
  protected internalMap: Map<Date, IRegistryRecord>;

  constructor() {
    this.internalMap = new Map<Date, IRegistryRecord>();
  }

  public clear(): void {
    this._clear();
    this.internalMap.clear();
  }

  public delete(key: Date): boolean {
    this._delete(key);
    return this.internalMap.delete(key);
  }

  public forEach(
    callbackfn: (
      value: IRegistryRecord,
      key: Date,
      map: Map<Date, IRegistryRecord>,
    ) => void,
    thisArg?: any,
  ): void {
    this.internalMap.forEach(callbackfn, thisArg);
  }

  public get(key: Date): IRegistryRecord | undefined {
    return this.internalMap.get(key);
  }

  public has(key: Date): boolean {
    return this.internalMap.has(key);
  }

  public set(key: Date, value: IRegistryRecord): this {
    this._set(key, value);
    this.internalMap.set(key, value);
    return this;
  }

  public [Symbol.iterator](): IterableIterator<[Date, IRegistryRecord]> {
    return this.internalMap[Symbol.iterator]();
  }

  public entries(): IterableIterator<[Date, IRegistryRecord]> {
    return this.internalMap.entries();
  }

  public keys(): IterableIterator<Date> {
    return this.internalMap.keys();
  }

  public values(): IterableIterator<IRegistryRecord> {
    return this.internalMap.values();
  }

  protected abstract _clear(): void;
  protected abstract _delete(key: Date): void;
  protected abstract _set(key: Date, value: IRegistryRecord): void;
}
