import fs from "fs";
import PQueue from "p-queue";
import {
  AbstractFileRegistry,
  IRegistryRecord,
  IRegistryRecordSerialized,
} from "./abstract-file-registry";

/**
 * Registry that stores data on the local disk
 */
export class FSFileRegistry extends AbstractFileRegistry {
  /**
   * Queue for write operations, 1 operation at a time
   */
  private writeQueue: PQueue;

  constructor(private filePath: string) {
    super();
    this.writeQueue = new PQueue({
      autoStart: true,
      concurrency: 1,
    });
    this.loadMapFromFile();
  }

  protected _clear(): void {
    this.writeMapToFile();
  }

  protected _delete(key: Date): void {
    this.writeMapToFile();
  }

  protected _set(key: Date, value: IRegistryRecord): void {
    this.writeMapToFile();
  }

  private loadMapFromFile() {
    this.internalMap = new Map<Date, IRegistryRecord>();

    try {
      const str = fs.readFileSync(this.filePath).toString();
      const inputData: {
        [dateStr: string]: IRegistryRecordSerialized;
      } = JSON.parse(str);

      for (const dateStr of Object.keys(inputData)) {
        const date = new Date(dateStr);
        const record = FSFileRegistry.deserializeRecord(inputData[dateStr]);
        this.internalMap.set(date, record);
      }
    } catch (e) {
      console.warn(
        "Registry file is corrupted or doesn't exist; creating empty registry",
      );
    }
  }

  /**
   * Write internal map data to file.
   * Performs a write if nothing is being written.
   * If writing is in progress, schedules another write at the end.
   * This is needed to save all writes that pile up during current write.
   */
  private writeMapToFile() {
    if (this.writeQueue.size < 2) {
      // serialize data
      const obj: { [key: string]: IRegistryRecordSerialized } = {};
      for (const [k, v] of this.internalMap) {
        obj[k.toDateString()] = FSFileRegistry.serializeRecord(v);
      }

      // stringify data
      const data: string = JSON.stringify(obj, null, 2);

      // write data to file
      this.writeQueue.add(
        () =>
          new Promise((resolve, reject) => {
            fs.writeFile(this.filePath, data, (err) =>
              err ? resolve() : reject(err),
            );
          }),
      );
    }
  }
}
