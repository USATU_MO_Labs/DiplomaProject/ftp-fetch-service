import {
  AbstractFileRegistry,
  IRegistryRecord,
} from "./abstract-file-registry";

/**
 * Registry that stores data in GCP Memorystore
 */
export class GCPFileRegistry extends AbstractFileRegistry {
  constructor() {
    super();
  }

  protected _clear(): void {
    // TODO
    throw new Error("Method not implemented.");
  }

  protected _delete(key: Date): void {
    // TODO
    throw new Error("Method not implemented.");
  }

  protected _set(key: Date, value: IRegistryRecord): void {
    // TODO
    throw new Error("Method not implemented.");
  }
}
