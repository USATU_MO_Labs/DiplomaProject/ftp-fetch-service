import Path from "path";
import { FileRegistry, FSFileRegistry } from "./file-registry";
import { IFile, IntermagnetFtpClient } from "./intermagnet-ftp-client";

async function main() {
  // create client
  const client = await IntermagnetFtpClient.create();

  // measure
  const startTime = Date.now();

  // fetch file list
  const files = await client.getRecursiveFlatList();
  const totalSize = files
    .map((f) => f.size)
    .reduce((prev, curr) => prev + curr, 0);

  console.log(`Total files: ${files.length}; total size: ${totalSize} bytes`);

  const endTime = Date.now();
  console.log(`File list downloaded in ${(endTime - startTime) / 1000}s`);

  // group files by date
  const dateContainers = new Map<Date, IFile[]>();
  for (const file of files) {
    const dateContainer = dateContainers.get(file.date);
    if (dateContainer) {
      dateContainer.push(file);
    } else {
      dateContainers.set(file.date, [file]);
    }
  }

  // sort keys by descending date
  const keys = Array.from(dateContainers.keys());
  keys.sort((a, b) => {
    if (a > b) {
      return -1;
    } else if (a < b) {
      return 1;
    } else {
      return 0;
    }
  });

  const registry: FileRegistry = new FSFileRegistry(
    Path.join(__dirname, "registry"),
  );

  // iterate over containers from latest to earliest
  for (const date of keys) {
    const dateContainer = dateContainers.get(date) as IFile[];

    // compare with registry, eliminate processed
    const registryRecord = registry.get(date);

    // TODO

    // send downloaded and unprocessed files to processing
    // TODO

    // queue downloads, add downloaded files to registry, send downloaded files to processing
    // TODO
  }

  // disconnect from ftp
  client.end();
}

main();
